<?php

class Kalkulator{

    private $angka1;
    private $angka2;

    public function __construct($angka1,$angka2)
    {
        $this->angka1 = $angka1;
        $this->angka2 = $angka2;
    }

    public function tambah(){
        $nilai =  $this->angka1 + $this->angka2;
        return $nilai;
    }

    public function kurang(){
        return $this->angka1 - $this->angka2;
    }

    public function bagi(){
        return $this->angka1 / $this->angka2;
    }

    public function kali(){
        return $this->angka1 * $this->angka2;
    }
}
?>