<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih function PHP</h1>


    <?php
 
     echo "<h3>Soal No 1 Greetings</h3>";

     $halo = new Perkenalan();
     $halo->greetings("bagas");
     $halo->greetings("feby");

     echo "<h3>Soal No 2</h3>";

    //manual reverse
    $halo->reverstring("Roti");

    //manual reverse
    $halo->reverstring("Pemrograman web");

     //fungsi bawaan dari php strrev
     echo strrev("Pemrograman pertama");

    ?>
</body>
</html>

<?php
   /***
     * Class Perkenalan
     */
    class Perkenalan{

        //code function disini
        function greetings($nama){
            echo "Halo " . $nama . ", Selamat datang di my website! <br/>";
        }

        function reverstring($kata1)
        {
            $balik = $this->reverse($kata1);
            echo $balik;
        }

        /***
         * fungsi untuk membalikan kalimat
         */
        function reverse($kata1)
        {
            //strlen berfungsi untuk menghitung length dari string
            $panjangstring = strlen($kata1);
            $tampung = "";
            for($i = $panjangstring-1; $i >=0; $i--)
            {
                $tampung .= $kata1[$i]; //mengambil tiap kata dari akhir sampai pertama
            }
            return $tampung . "<br/>" ;
        }


     }
?>